
@servers(['web' => ['master_bndjsjfbbz@139.59.254.38']])
@setup
    $root_app_dir = '/home/987847.cloudwaysapps.com/hxkwkpwzbk';
    $releases_dir = $root_app_dir .'/' . 'private_html'; 
    $release = date('YmdHis');  
    $current_release_dir = $releases_dir .'/'. $release;
    $repository = 'git@gitlab.com:nayjel/segworks.git';
    $webroot = 'webroot';
@endsetup
@story('deploy')
    clone_repository
    run_composer
    update_symlinks
@endstory
@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $current_release_dir }}
    cd {{ $current_release_dir }}
    git reset --hard {{ $commit }}
@endtask
@task('run_composer')
    echo "Deploying ({{ $release }})"
    cd {{ $current_release_dir }}
    composer install --prefer-dist --no-scripts -q -o
@endtask
@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $current_release_dir }}/storage
    [ -d {{ $releases_dir }}/storage ] || mkdir {{ $releases_dir }}/storage {{ $releases_dir }}/storage/logs {{ $releases_dir }}/storage/framework {{ $releases_dir }}/storage/framework/cache {{ $releases_dir }}/storage/framework/sessions {{ $releases_dir }}/storage/framework/views
    [ -d {{ $current_release_dir }}/bootstrap ] || mkdir {{ $current_release_dir }}/bootstrap
    ln -nfs {{ $releases_dir }}/storage {{ $current_release_dir }}/storage
    chown -R $USER:www-data {{ $current_release_dir }}/storage {{ $current_release_dir }}/bootstrap
    echo 'Linking .env:'
    ln -nfs {{ $releases_dir }}/.env {{ $current_release_dir }}/.env
    echo 'Linking current release:'
    ln -nfs {{ $current_release_dir }} {{ $root_app_dir }}/public_html/{{ $webroot }}
@endtask